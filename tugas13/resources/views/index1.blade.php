@extends('main')
@section('title')
Tugas Ke 14   
@endsection
@section('subtitle')
Index
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary sm">Tambah Data</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Ket</th>
        <th scope="col">Opsi</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($show as $s => $value)
      <tr>
        <th scope="row">{{$s+1}}</th>
        <td>{{$value->nama}}</td>
        <td>{{$value->umur}}</td>
        <td>{{$value->bio}}</td>
        <td>
          <a href="/cast/{{$value->id}}" class="btn btn-primary sm">Detail</a>
          <a href="/cast/{{$value->id}}/edit" class="btn btn-success sm">Edit</a>
        </td>
        <td> 
          <form action="/cast/{{$value->id}}" method="POST">
            @csrf
            @method('delete')
          <input type="submit" class="btn btn-danger" value="Delete">
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table> 
@endsection