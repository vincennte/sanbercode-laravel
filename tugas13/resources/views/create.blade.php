@extends('main')
@section('title')
Tugas Ke 14   
@endsection
@section('subtitle')
Tambah Data  
@endsection

@section('content')  
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control" name="nama">
      @error('nama')
    <div class="alert alert-danger">{{$message}}</div>
      @enderror
    </div>
    <div class="form-group">
      <label>Umur</label>
      <input type="text" class="form-control" name="umur">
      @error('umur')
      <div class="alert alert-danger">{{$message}}</div>
      @enderror
    </div>
    <div class="form-group">
        <label>Biodata</label>
      <textarea class="form-control" name="bio"></textarea>
      @error('bio')
      <div class="alert alert-danger">{{$message}}</div>
      @enderror
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection