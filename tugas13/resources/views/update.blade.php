@extends('main')
@section('title')
Tugas Ke 14   
@endsection
@section('subtitle')
Edit Cast  
@endsection

@section('content')  
<form action="/cast/{{$find->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control" name="nama" value="{{$find->nama}}">
      @error('nama')
    <div class="alert alert-danger">{{$message}}</div>
      @enderror
    </div>
    <div class="form-group">
      <label>Umur</label>
      <input type="text" class="form-control" name="umur" value="{{$find->umur}}">
      @error('umur')
      <div class="alert alert-danger">{{$message}}</div>
      @enderror
    </div>
    <div class="form-group">
        <label>Biodata</label>
      <textarea class="form-control" name="bio">{{$find->bio}}</textarea>
      @error('bio')
      <div class="alert alert-danger">{{$message}}</div>
      @enderror
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection