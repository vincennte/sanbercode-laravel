<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class castController extends Controller
{
    public function index1(){
        $show = DB::table('cast')->get();
        return view('index1',['show' => $show]);
    }
    public function show($id){
        $detail = DB::table('cast')->find($id);
        return view('detail',['detail' => $detail]);
    }
    public function create() {
        return view('create');
    }
    public function store(Request $request){
        $validated = $request->validate([
            'nama' => 'required',
            'umur' => 'required|numeric',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Nama Harus Diisi',
            'umur.numeric' => 'Harus berupa angka',
            'umur.required' => 'Umur Harus Diisi',
            'bio.required' => 'Bio Harus Diisi',

        ]
        );
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
        return redirect('/cast');

    }
    public function edit($id){
        $find = DB::table('cast')->find($id);
        return view('update',['find' => $find]);
    }
    public function update($id, Request $request){
        DB::table('cast')
              ->where('id', $id)
              ->update(
                [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
                ]
            );
        return redirect('/cast');
    }
    public function destroy($id){
        DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}
