<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\castController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/table',function(){
    return view('table');
});
Route::get('/data-table',function(){
    return view('data-table');
});


//tugas 15
Route::get('/cast',[castController::class,'index1']);
Route::get('/cast/create',[castController::class,'create']);
Route::post('/cast',[castController::class,'store']);
Route::get('/cast/{cast_id}/edit',[castController::class,'edit']);
Route::get('/cast/{cast_id}',[castController::class,'show']);
Route::put('/cast/{cast_id}',[castController::class,'update']);
Route::delete('/cast/{cast_id}',[castController::class,'destroy']);
