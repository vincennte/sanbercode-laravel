<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function registrasi(){
        return view('register');
    }
    public function kirim(Request $request){
        $namaDepan=$request['FirstName'];
        $namaBelakang=$request['LastName'];
        return view('welcome', ['namaDepan'=>$namaDepan, 'namaBelakang'=>$namaBelakang]);
    }
}
