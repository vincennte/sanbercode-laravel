<!DOCTYPE HTML>
<html>
	<head>
		<title>Tugas 1</title>
	</head>
	<body>
        <form action="/welcome" method="POST">
        @csrf
            <h2>Buat Account Baru!</h2>
            <h3>Sign Up Form</h3>
            <p>First Name: </p>
            <input type="text" name="FirstName">
            <p>Last Name: </p>
            <input type="text" name="LastName">
            <p>Gender: </p>
            <input type="radio" value="male">
                <label for="male">Male</label> <br>
            <input type="radio" value="Female">
                <label for="female">Female</label> <br>
            <input type="radio" value="othergender">
                <label for="other">Other</label> <br>
            <p>Nationality:</p>
            <select name="nationality">
                <option value="indonesia">Indonesia</option>
                <option value="singapura">Singapura</option>
                <option value="malaysia">Malaysia</option>
            </select>
            <p>Language Spoken:</p>
            <input type="checkbox" value="indonesia">
                <label for="indonesia">Bahasa Indonesia</label><br>
            <input type="checkbox" value="inggris">
                <label for="indonesia">Bahasa Inggris</label><br>
            <input type="checkbox" value="otherlanguage">
                <label for="indonesia">Other</label><br>
            <p>Bio: </p>
            <textarea name="" rows="7" cols="40">
            </textarea> <br>
            
                <input type="submit">
        </form>
	</body>
</html>